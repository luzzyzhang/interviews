==============
Preface
==============

**Growth mindset and Iteration**

**Hacker**

.. contents::


训练计划
=========

- Target
- Prepare
- Plan&Time
- Resources

1. Read `CS Notes`_
2. Read `Interview reference`_

编码技艺
========

编程语言
----------------
* Python 语言问题整理
* `Data strucures and Algorithms in Python`
* Go 语言问题整理
* Go tour -> Effective Go

数据结构与算法
----------------
* 总结算法题目
* 算法题目练习
* `Algorithms in Python`_

系统设计
========

- `System design primer`_
- `Awesome Scalability`_

数据库
---------
- MySQL
- Redis
- `Database A`_
- `Database B`_

基础组件
---------


基础知识
========

- 操作系统
- 网络基础
- `What happen when`_

项目剖析
========

- 丰富完善博客
- 做过的项目与使用的组件讲清楚讲明白
- 个人项目
    1. 实现 Redis 分布式锁（`Distributed locks with Redis`_）

经典问题
========

模拟面试
========

参考资料
========

- `CS Notes`_
- `Interview reference`_
- `Coding university`_
- `Tech interview handbook`_
- `Google Interview Tips`_
- `Everything you need to know to get the job`_
- `Interview Resource`_
- `Interview`_
- `Awesome Interview Questions`_
- `Geeks For Geeks`_
- `Interview Waking Up`_
- `LeetCode Solutions1`_
- `LeetCode Solutions2`_


.. _CS Notes: https://github.com/CyC2018/CS-Notes
.. _Interview reference: https://github.com/0voice/interview_internal_reference
.. _Coding university: https://github.com/jwasham/coding-interview-university
.. _Tech interview handbook: https://yangshun.github.io/tech-interview-handbook/
.. _Google Interview Tips: https://careers.google.com/how-we-hire/interview/
.. _Everything you need to know to get the job: https://github.com/kdn251/interviews
.. _Distributed locks with Redis: https://redis.io/topics/distlock
.. _Interview Resource: https://github.com/kdn251/interviews/blob/master/README-zh-cn.md
.. _Database A: https://github.com/huihut/interview#-%E6%95%B0%E6%8D%AE%E5%BA%93
.. _Database B: https://github.com/MaximAbramchuck/awesome-interview-questions#database-technologies
.. _Interview: https://github.com/huihut/interview
.. _System design primer: https://github.com/donnemartin/system-design-primer
.. _Algorithms in Python: https://github.com/TheAlgorithms/Python
.. _Awesome Interview Questions: https://github.com/MaximAbramchuck/awesome-interview-questions#database-technologies
.. _Awesome Scalability: https://github.com/binhnguyennus/awesome-scalability
.. _What happen when: https://github.com/alex/what-happens-when
.. _Geeks For Geeks: https://www.geeksforgeeks.org/
.. _Interview Waking Up: https://github.com/wolverinn/Waking-Up
.. _LeetCode Solutions1: https://github.com/azl397985856/leetcode
.. _LeetCode Solutions2: https://github.com/labuladong/fucking-algorithm
