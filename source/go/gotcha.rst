================================
Must know questions about Golang
================================

.. contents::

Update from ``range``
=====================

.. note::
    The data values generated in the ``range`` clause are copies of the actual collection elements.
    They are not references to the original items.
    This means that updating the values will not change the original data.
    It also means that taking the address of the values will not give you pointers to the original data.

.. literalinclude:: example/range_update_item.go
    :language: go

Iteration Variables and Closures in ``for`` Statements
======================================================

.. note::
    This is the most common gotcha in Go. The iteration variables in for statements are reused in each iteration.
    This means that each closure (aka function literal) created in your for loop will reference the same variable
    (and they'll get that variable's value at the time those goroutines start executing).

.. literalinclude:: example/closure_for.go
    :language: go


Array vs Slice
==============
- `Go slice useage and internal`_

Map 顺序访问
============

Goroutine 原理
==============



References
==========

- `Golang common mistakes`_
- `Golang新开发者要注意的陷阱和常见错误`_


.. _Golang新开发者要注意的陷阱和常见错误: https://colobu.com/2015/09/07/gotchas-and-common-mistakes-in-go-golang/#
.. _Golang common mistakes: http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/
.. _Go slice useage and internal: https://blog.golang.org/go-slices-usage-and-internals
