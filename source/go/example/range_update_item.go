package main

import "fmt"

func bad() {
	data := []int{1, 2, 3}
	for _, v := range data {
		v *= 10 //original item is not changed
	}

	fmt.Println("data:", data) //prints data: [1 2 3]
}

func good() {
	data := []int{1, 2, 3}

	for i := range data {
		data[i] *= 10
	}

	fmt.Println("data:", data) //prints data: [10 20 30]
}

// If your collection holds pointer values then the rules are slightly different.
// You still need to use the index operator if you want the original record to point
// to another value, but you can update the data stored at the target location using
//  the second value in the "for range" clause.
func updateData() {
	data := []*struct{ num int }{{1}, {2}, {3}}

	for _, v := range data {
		v.num *= 10
	}
	fmt.Println(data[0], data[1], data[2]) //prints &{10} &{20} &{30}
}

func main() {
	fmt.Println("Bad:")
	bad()
	fmt.Println("Good:")
	good()
	fmt.Println("Update pointer collection data:")
	updateData()
}
