package main

import "fmt"

func factorial(x uint) uint {
	if x == 0 {
		return 1
	}

	return x * factorial(x-1)
}

func fac2(x int) int {
	r := 1

	for i := 1; i <= x; i++ {
		r *= i

	}

	return r
}

func main() {
	x := uint(5)
	calcFactorial := factorial(x)
	fmt.Println(calcFactorial)
	fmt.Println(fac2(5))
	fmt.Println(fac2(5))
	fmt.Println(factorial(15))
	fmt.Println(fac2(15))
}
