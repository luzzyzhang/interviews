package main

import "fmt"

func loop() {
	for i := 0; i < 10; i++ {
		fmt.Printf("%d", i)
	}
	s := make([] byte, 1)
}

func main() {
	go loop()
	loop()
}

