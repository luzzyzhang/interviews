===================
Problems collection
===================

.. contents::


Some questions
===============

1. 编写 Golang 脚本从搜狐首页（http://www.sohu.com）抓取页面的所有图片，
   并且在当前目录下按日期（年月日）新建子目录保存抓取下来的图片。

2. 现在有一个 nginx log 文件 A，使用 Linux 命令从文件 A 中计算包含 “image.gif" 的行数，并算出最大 QPS。

3. 用 Golang 实现 ``n!``。

4. 有 10 张图片，其中的 2 张除了分辨率之外完全是一样的，从 10 张图片种找出这两张相似的图片

Go 内存分配
===========

垃圾回收
========

调度器(MPG)
============

- `Go scheduler`_

Go 并发模型
============

CSP vs Actor

References
==========

- `Golang1`_
- `Golang2`_
- `Golang3`_
- `Golang4`_


.. _Golang1: https://github.com/KeKe-Li/data-structures-questions/blob/master/src/chapter05/golang.01.md
.. _Golang2: https://gist.github.com/huyi1985/8a81449a0490d6d4c33eb2151e89cfc5
.. _Golang3: http://interview.wzcu.com/Golang/%E5%9F%BA%E7%A1%80.html
.. _Golang4: https://github.com/lifei6671/interview-go
.. _Go scheduler: https://colobu.com/2017/05/04/go-scheduler/