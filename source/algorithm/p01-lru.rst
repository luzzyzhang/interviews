=========
LRU Cache
=========

Data Structure to implement LRU Cache
=====================================

- Double link list
- Hash map


References
==========

- Python ``@lru_cache``
- Golang LRU Cache
