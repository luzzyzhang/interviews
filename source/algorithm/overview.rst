==========
Algorithms
==========

``Algorithms + Data Structures = Programs``

.. epigraph::
  Bad programmers worry about the code.
  Good programmers worry about data structures and their relationships.

  -- Linus Torvalds

How to solve it
---------------
1. Understand the problem: input, output, condition
2. Devise a plan
3. Carry out the plan
4. Look back


- `Tree Search`_
- `Data Structure`_

.. _Tree Search: https://zh.wikipedia.org/wiki/%E6%A0%91%E7%9A%84%E9%81%8D%E5%8E%86

.. _Data Structure: https://github.com/wangzheng0822/algo
