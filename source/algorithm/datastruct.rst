==================
Data Strucutres
==================

.. contents::

Hash Table
==========


References
----------

- `Hash Table`_

Skip List
=========

References
----------

- `Skip List Done Right`_
- `Skip List`_


.. _Hash Talbe: https://en.wikipedia.org/wiki/Hash_table
.. _Skip List: https://en.wikipedia.org/wiki/Skip_list
.. _Skip List Done Right: http://ticki.github.io/blog/skip-lists-done-right/
