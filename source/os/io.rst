
=======
IO 模型
=======

.. contents::

五种 IO 模型比较
================

.. image:: https://camo.githubusercontent.com/d89aed2ba6c5390aad0626b013c288d8849c4f39/68747470733a2f2f63732d6e6f7465732d313235363130393739362e636f732e61702d6775616e677a686f752e6d7971636c6f75642e636f6d2f313439323932383130353739315f332e706e67


``epoll``
=========

- `epoll`_
- ``ET`` vs ``LT``
    1. `水平触发边沿触发:`_




参考
=======

- `IO 模型`_
- `完全理解同步/异步与阻塞/非阻塞`_
- `怎样理解阻塞非阻塞同步异步区别`_


.. _完全理解同步/异步与阻塞/非阻塞: https://zhuanlan.zhihu.com/p/22707398?utm_source=wechat_session&utm_medium=social&utm_oi=1114155040998248448&from=singlemessage&isappinstalled=0
.. _怎样理解阻塞非阻塞同步异步区别: https://www.zhihu.com/question/19732473/answer/241673170
.. _IO 模型: https://github.com/CyC2018/CS-Notes/blob/master/notes/Socket.md
.. _水平触发边沿触发: https://blog.csdn.net/lihao21/article/details/67631516
.. _epoll: http://man7.org/linux/man-pages/man7/epoll.7.html
