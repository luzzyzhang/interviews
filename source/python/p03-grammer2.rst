==============
语言相关问题 2
==============

.. contents:: 目录
    :depth: 2

Monkey Patching
=================

- What is monkey patching?
- How to use in Python? Example?
- Is it ever a good idea?


How are Dict and Set implemented internally?
=============================================

- `Internal dict <https://docs.python.org/3/faq/design.html#how-are-dictionaries-implemented-in-cpython>`_
- `Why must dict keys be immutable <https://docs.python.org/3/faq/design.html#why-must-dictionary-keys-be-immutable>`_
- `Internal set <https://stackoverflow.com/questions/3949310/how-is-set-implemented>`_

How are list implemented in CPython?
====================================

- `Internal list <https://docs.python.org/3/faq/design.html#how-are-lists-implemented-in-cpython>`_


What is MRO in Python? How does it work?
========================================

What are descriptors?
==========================

- Is there a difference between a descriptor and a decorator?

How memory is managed in Python?
==================================

- `How manage memory`_
- Python memory is managed by Python private heap space. All Python objects and
  data structures are located in a private heap. The programmer does not have
  an access to this private heap and interpreter takes care of this Python
  private heap.
- The allocation of Python heap space for Python objects is done by Python
  memory manager. The core API gives access to some tools for the programmer
  to code.
- Python also have an inbuilt garbage collector, which recycle all the
  unused memory and frees the memory and makes it available to the heap space.

  .. image:: https://assets.interviewbit.com/assets/skill_interview_questions/python/python-memory-manager-06d44b2add8b64c72f76994b4c759096df11a75a5b32b611da3371b8c6f0c7e6.png.gz


Why isn’t all memory freed when CPython exits?
==================================================

- `Memoery Freed <https://docs.python.org/3/faq/design.html#why-isn-t-all-memory-freed-when-cpython-exits>`_

Why are there separate tuple and list data types?
===================================================

- `List vs Tuple <https://docs.python.org/3/faq/design.html#why-are-there-separate-tuple-and-list-data-types>`_

What is output?
==================

.. code-block:: python

    >>> -12 % 10
    >>> -12 // 10

What is the difference between Python 2 and 3?
==============================================

What are metaclasses in Python
===============================

Write ``timeit`` decorator for measure time of function execution.
===================================================================

.. code-block:: python

    import time
    from functools import wraps

    def timeit(func):
        @wraps(func)
        def _(*args, **kwargs):
            s = time.perf_counter()
            r = func(*args, **kwargs)
            e = time.perf_counter()
            print(e-s)
            return r
        rerturn _



Make a prime number list from (1,100)
=====================================

What do these mean to you: ``@classmethod``, ``@staticmethod``, ``@property``?
==============================================================================







Reference
===========



.. _How manage memory: https://docs.python.org/3/faq/design.html#how-does-python-manage-memory
