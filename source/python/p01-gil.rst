===========
GIL
===========

.. contents::


Problem
========

- What is GIL?
- Why GIL?
- When?
- How to release and requeire GIL?

Solution
=========

`Global Interpreter Lock` is a **mutex** that protects access to Python
objects, preventing multiple threads from executing Python bytecodes at once.
That allows only one thread to hold the control of the Python interpreter.
This lock is necessary mainly because CPython's memory management is not
thread-safe.


"One thread runs Python, while N others sleep or await I/O."


Discussion
===========


Reference
=========

- `Python GIL`_
- `GlobalInterpreterLock`_
- `Grok GIL`_



.. _Python GIL: https://realpython.com/python-gil/
.. _GlobalInterpreterLock: https://wiki.python.org/moin/GlobalInterpreterLock
.. _Grok GIL: https://opensource.com/article/17/4/grok-gil
