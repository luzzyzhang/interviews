======================================================
How to prepare troubleshooting and scripting interview
======================================================

.. note::

    `Prepare Google interview: Troubleshoting and Scripting`_

.. contents::


Communication and Collaboration
===============================

- Think out loud
- It's key to how we approach developing and building our products
- We don't just want to see the final solution
  we want to undestand how you arrive at it
- Teams have a mission to work together to solve big but often ambiguous
  problems, not just write code
- How you plan to solve the problem
- Be sure to ask questions
- And remember: we are not looking for one specific answer. Some of the
  problems will be delibrerately open-ended.
- Real engineering problems require you to dig deeper

Troubleshooting
===============

- Knowing how to troubleshooting a given scenario using a series of logical
  steps is key to addressing the needs of users while waiting our brand
- Troubleshooting a problem:
    1. Examine:
        a. You should examine and understand the problem statement before
           jumping into how to troubleshooting it
        b. Verify, reproduce and scope issues
        c. Ask carify questions, think out loud and step us through the process
           from beginning
        d. Use the whiteboard or a pad of paper to gather your thoughts
    2. Diagnose:
        a. Hypothesize cases
        b. Test
        c. Iterate
    3. Test or treat

Scripting
=========

- Focus on problem solving
- Idimoatic code that's both maintainase and readable
- Is the code easy to undestand?
- Can you test the code?
- Can the code handle erros?

Best practices
==============

Explain:
    We want to undestand how you think, so it's important to explain your
    thought process during the interview

Carify:
    We're not only evaluating your technical ability but also how you sovle
    problems. May questions will be deliberately open-ended to give us an idea
    of how you solve the technical problems. We encorage you to ask for
    clarification.

Improve:
    And We all know that our first solution may not be the best one.
    So, once you've come up with an answer to a question, think about ways to
    improve upon it and left your interviewer know what yor're thinking.

Practice:
    Practice on paper or a whiteboard.


.. _`Prepare Google interview: Troubleshoting and Scripting`:
    https://www.youtube.com/watch?v=_8cH-QPVXsw
