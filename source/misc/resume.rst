==============
Resume guide
==============

工作项目经历
==============

原则::

    工作经验部分最主要的目的，就是展示你在成熟机构工作时候的影响力和价值

    描述做过的事情、怎么做的、以及结果如何。理想的做法是尽可能的量化结果。

公式::

    “Accomplished [X] as measured by [Y] by doing [Z]”

    “通过 [Z] 实现了 [X] 的效果，提高或者改善（衡量标准）[Y]”

例子::

    1. 通过集成油量表传感器并设置电池节电状态，将设备的电池寿命提高了 8%
      - 实现了 [X] —— 提高了设备的电池寿命
      - 提高或者改善了 [Y] —— 百分之 8
      - 通过 [Z] —— 通过集成油量表传感器并设置电池节电状态
    2. Increase server query response time by 15% by restructuring API
    3. 通过实施分布式缓存功能减少了 75% 的对象渲染时间，从而使得用户登录速度加快了 10%
    4. 实现了一种新的基于 windiff 的比较算法，系统平均匹配精度由 1.2 提升到 1.5

词库::

    Reduced _____ by _____ by _____.
    Redesigned _____ for _____.
    Implemented _____ for _____ by _____.
    Improved _____ by _____ through _____.
    Utilized _____ to _____ for _____.
    Increased _____ by _____ through _____.
    Integrated _____ by _____ for _____.
    Incorporated _____ for _____ by _____.

    通过 _____ 降低了 _____ 至 _____。
    为了 _____ 重新设计了 _____ 。
    为了 _____ 通过 _____ 完成了 _____。
    通过 _____ 优化了 _____ 为 _____。
    为了 _____ 将 _____ 应用到了 _____。
    通过 _____ 提高了 _____ 至 _____。
    为了 _____ 通过 _____ 将 _____ 集成。
    为了 _____ 通过 _____ 成立了 _____ 。

.. image:: https://cdn-media-1.freecodecamp.org/images/08UNCYv26D5hz87HF-XkHIzD-G1PBj6X0PsE
