================
Questions to ask
================

Reference
=========

- `Questions to ask`_
- `Reverse Interview`

General
=======

Culture
=======

- How is Job Performance Measured?

Technical
=========

- 开发流程
- 是否有单元测试
- 如何控制代码质量

Product
=======

Management
==========

Leadership
==========

HR
===

.. _Questions to ask: https://yangshun.github.io/tech-interview-handbook/questions-to-ask
.. _Reverse Interview: https://github.com/yifeikong/reverse-interview-zh

