==========
Data Types
==========

.. contents::

Common Problems
================

1. `VARCHAR` v.s. `CHAR`
    - `The CHAR and VARCHAR Types`_
    - `What is the difference between VARCHAR and CHAR`_

Numeric Types
=============

Date and Time Types
====================

String Types
=============

Spatial Data Types
===================

The JSON Data Type
==================


References
==================

- `MySQL Data Types`_


.. _MySQL Data Types: https://dev.mysql.com/doc/refman/8.0/en/data-types.html
.. _The CHAR and VARCHAR Types: https://dev.mysql.com/doc/refman/8.0/en/char.html
.. _What is the difference between VARCHAR and CHAR: https://stackoverflow.com/questions/1885630/whats-the-difference-between-varchar-and-char

