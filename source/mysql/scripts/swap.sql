DROP TABLE IF EXISTS salary;
CREATE TABLE salary (
  id INT,
  name VARCHAR (100),
  sex CHAR (1),
  salary INT
);
INSERT INTO
  salary (id, name, sex, salary)
VALUES
  ('1', 'A', 'm', '2500'),
  ('2', 'B', 'f', '1500'),
  ('3', 'C', 'm', '5500'),
  ('4', 'D', 'f', '500');