========================
数据库事务四个特性与含义
========================

数据库事务 ``transanction`` 正确执行的四个基本要素(*ACID*):
    - 原子性(``Atomicity``)
    - 一致性(``Consistency``)
    - 隔离性(``Isolation``)
    - 持久性(``Durability``)

**原子性**:
    整个事务中的所有操作，要么全部完成，要么全部不完成，不可能停滞在中间某个环节。
    事务在执行过程中发生错误，会被回滚（Rollback）到事务开始前的状态，
    就像这个事务从来没有执行过一样。

**一致性**:
    在事务开始之前和事务结束以后，数据库的完整性约束没有被破坏。
    参考 `Consistency failure`_

**隔离性**:
    定义了数据库系统中一个操作的结果在何时以何种方式对其他并发操作可见

    ::

        隔离状态执行事务，使它们好像是系统在给定时间内执行的唯一操作。
        如果有两个事务，运行在相同的时间内，执行相同的功能，
        事务的隔离性将确保每一事务在系统中认为只有该事务在使用系统。
        这种属性有时称为串行化，为了防止事务操作间的混淆，
        必须串行化或序列化请求，使得在同一时间仅有一个请求用于同一数据。
        数据库允许多个并发事务同时对其数据进行读写和修改的能力，
        隔离性可以防止多个事务并发执行时由于交叉执行而导致数据的不一致。

    事务隔离分为不同级别包括
      - 串行化（Serializable）
      - 可重复读（Repeatable read）（MySQL 默认的隔离级别）
      - 提交读（Read committed）
      - 未提交读（Read uncommitted）

    读现象
        - 脏读
        - 不可重复读
        - 幻读

    // TODO: 完善读现象总结
    脏读如果事务2回滚了, 不可重复读事务2正常提交了


| 例子：`Isolation failure`_
| 说明：`Isolation`_, `事务隔离`_

**持久性**:
    在事务完成以后，该事务所对数据库所作的更改便持久的保存在数据库之中，并不会被回滚。

参考：
    - Wikipedia `ACID`_
    - 事务 `Transaction`_
    - `Race Conditions/Concurrency Defects in Databases`_


.. _ACID: https://en.wikipedia.org/wiki/ACID
.. _Consistency failure: https://en.wikipedia.org/wiki/ACID#Consistency_failure
.. _Isolation failure: https://en.wikipedia.org/wiki/ACID#Isolation_failure
.. _Isolation: https://en.wikipedia.org/wiki/Isolation_(database_systems)
.. _Transaction: https://github.com/CyC2018/CS-Notes/blob/master/notes/%E6%95%B0%E6%8D%AE%E5%BA%93%E7%B3%BB%E7%BB%9F%E5%8E%9F%E7%90%86.md#%E4%B8%80%E4%BA%8B%E5%8A%A1
.. _事务隔离: https://zh.wikipedia.org/wiki/%E4%BA%8B%E5%8B%99%E9%9A%94%E9%9B%A2
.. _Race Conditions/Concurrency Defects in Databases: https://ketanbhatt.com/db-concurrency-defects/

