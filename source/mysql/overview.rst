======================
Everything about MySQL
======================

Questions
=========

1. SQL 优化
2. 联合索引命中规则
3. 事务
4. 锁
5. 分布式锁

References
==========

- `MySQL interview1`_
- `Database`_
- `Database A`_
- `Database B`_


.. _MySQL interview1: https://github.com/0voice/interview_internal_reference#9
.. _Database: https://github.com/CyC2018/CS-Notes#floppy_disk-%E6%95%B0%E6%8D%AE%E5%BA%93
.. _Database A: https://github.com/huihut/interview#-%E6%95%B0%E6%8D%AE%E5%BA%93
.. _Database B: https://github.com/MaximAbramchuck/awesome-interview-questions#database-technologies
