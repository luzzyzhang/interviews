==============
Sharding
==============

TODO: Summary everything about sharding and partitioning


References
==========

1. `Sharding Pinterest How we scaled our MySQL fleet`_
2. `Understanding database sharding`_
3. `Overview of partitioning in MySQL`_
4. `MySQL分区与传统的分库分表`_ (√)
5. `大众点评订单系统分库分表实践`_ (√)
6. `达达高性能服务端优化之路`_ (√)
7. `MySQL 最佳实践分区表基本类型`_
8. `MySQL Sharding at Quora`_


.. _Sharding Pinterest How we scaled our MySQL fleet: https://medium.com/pinterest-engineering/sharding-pinterest-how-we-scaled-our-mysql-fleet-3f341e96ca6f
.. _Understanding database sharding: https://www.digitalocean.com/community/tutorials/understanding-database-sharding
.. _Overview of Partitioning in MySQL: https://dev.mysql.com/doc/refman/8.0/en/partitioning-overview.html
.. _MySQL分区与传统的分库分表: http://haitian299.github.io/2016/05/26/mysql-partitioning/
.. _大众点评订单系统分库分表实践: https://tech.meituan.com/2016/11/18/dianping-order-db-sharding.html
.. _达达高性能服务端优化之路: https://www.infoq.cn/article/imdada-high-performance-server-optimization/
.. _MySQL 最佳实践分区表基本类型: http://mysql.taobao.org/monthly/2017/11/09/
.. _MySQL Sharding at Quora: https://www.quora.com/q/quoraengineering/MySQL-sharding-at-Quora
