=====================
LeetCode sql problems
=====================

.. contents::


Questions
============

1. Swap Salary
2. Duplicate Emails
3. Delete Duplicate Emails


How To Find Duplicate Values in MySQL
-------------------------------------

`Find duplicate rows`_

1. Prepare sample table

   .. literalinclude:: ./scripts/contact.sql
       :language: sql

2. Query data

  .. code-block:: sql

    SELECT * FROM contacts ORDER BY email;

3. Find duplicate values in one column

  .. code-block:: sql

    SELECT email, COUNT(email)
    FROM
        contacts
    GROUP BY email
    HAVING COUNT(email) > 1;

4. Find duplicate values in multiple columns

  .. code-block:: sql

    SELECT
        first_name, COUNT(first_name),
        last_name,  COUNT(last_name),
        email,      COUNT(email)
    FROM
        contacts
    GROUP BY
        first_name,
        last_name,
        email
    HAVING
        COUNT(first_name) > 1
        AND COUNT(last_name) > 1
        AND COUNT(email) > 1;



How To Delete Duplicate Rows in MySQL
-------------------------------------

1. Prepare sample data

   .. literalinclude:: ./scripts/contact.sql
       :language: sql

2.  Duplicate emails in the table

  .. code-block:: sql

    SELECT
        email, COUNT(email)
    FROM
        contacts
    GROUP BY
        email
    HAVING
        COUNT(email) > 1;

3. Delete duplicate rows

  A) Delete duplicate rows using ``DELETE JOIN`` statement

    .. code-block:: sql

      -- Query first delete second

      SELECT t1.* FROM contacts t1
      INNER JOIN contacts t2
      WHERE
          t1.id < t2.id AND
          t1.email = t2.email;

      -- Delete duplicate rows and keeps the highest id

      DELETE t1 FROM contacts t1
      INNER JOIN contacts t2
      WHERE
          t1.id < t2.id AND
          t1.email = t2.email;

      -- Delete duplicate rows and keep the lowest id

      DELETE c1 FROM contacts c1
      INNER JOIN contacts c2
      WHERE
          c1.id > c2.id AND
          c1.email = c2.email;

  B) Delete duplicate rows using an intermediate table

    ::

      1. Create a new table with the structure the same as the original table that you want to delete duplicate rows.
      2. Insert distinct rows from the original table to the immediate table.
      3. Drop the original table and rename the immediate table to the original table.

    .. code-block:: sql

      -- step 1
      CREATE TABLE contacts_temp
      LIKE contacts;

      -- step 2 Only work for disabled sql_mode=only_full_group_by
      INSERT INTO contacts_temp
      SELECT *
      FROM contacts
      GROUP BY email;

      -- step 3
      DROP TABLE contacts;

      ALTER TABLE contacts_temp
      RENAME TO contacts;

  C) Delete duplicate rows using the ``ROW_NUMBER()`` function

    .. note::
      ``ROW_NUMBER()`` function has been supported since MySQL version 8.02 so
      you should check your MySQL version before using the function.


    - The following statement uses the ``ROW_NUMBER()`` function to assign a sequential integer number to each row. If the email is duplicate, the row number will be greater than one.

    .. code-block:: sql

      SELECT
        id,
        email,
      ROW_NUMBER() OVER (
        PARTITION BY email
        ORDER BY email
      ) AS row_num
      FROM contacts;

    - The following statement returns id list of the duplicate rows:

    .. code-block:: sql

      SELECT
          id
      FROM (
          SELECT
              id,
              ROW_NUMBER() OVER (PARTITION BY email ORDER BY email) AS row_num
          FROM
              contacts
      ) t
      WHERE
          row_num > 1;

    - Delete the duplicate rows from the contacts table using the DELETE statement with a subquery in the WHERE clause:

    .. code-block:: sql

      DELETE FROM contacts
      WHERE
          id IN (
          SELECT
              id
          FROM (
              SELECT
                  id,
                  ROW_NUMBER() OVER (
                      PARTITION BY email
                      ORDER BY email) AS row_num
              FROM
                  contacts

          ) t
          WHERE row_num > 1
      );




References
===========

- `LeetCode 20`_
- `Find duplicate rows`_
- `How To Delete Duplicate Rows in MySQL`_

.. _LeetCode 20: https:--github.com/CyC2018/CS-Notes/blob/master/notes/Leetcode-Database%20%E9%A2%98%E8%A7%A3.md
.. _Find duplicate rows: http:--www.mysqltutorial.org/mysql-find-duplicate-values/
.. _How To Delete Duplicate Rows in MySQL: http:--www.mysqltutorial.org/mysql-delete-duplicate-rows/

