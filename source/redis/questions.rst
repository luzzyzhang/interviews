==========================
Some questions about redis
==========================

.. contents::

Why redis single thread
=======================

- `Why Redis single thread`_
    1. Operation in memory
    2. Dev and maintain easy keep simple
    3. IO bound not CPU bound
    4. IO Multiplexing

- Redis 事务 and Redis 事务存在的问题

- 雪崩击穿 & 穿透

- Redis 常见问题


.. _Why Redis single thread: https://draveness.me/whys-the-design-redis-single-thread
