.. hiker documentation master file, created by
   sphinx-quickstart on Sun Jul 28 09:17:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

绝地武士修炼指南
=================================


- 管闚锥指
- **Practice makes perfect**

.. toctree::
   :maxdepth: 1
   :glob:

   preface.rst
   chapters/*

